<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {

			try{
				// TODO: Create PDO connection
				$dsn = 'mysql:host='. DB_HOST .';dbname='. DB_NAME;
				$this->db = new PDO($dsn, DB_USER, DB_PWD);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				throw $e;
			}

        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
		try {

			$eventList = array();
			$tmpEvLst = array();
			
			$sql = 'SELECT * FROM event';
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			$tmpEvLst = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($tmpEvLst as $row) {
				$eventList[$row['id']-1] = new Event($row['title'], $row['date'], $row['description'], $row['id']);
			}
			
			return $eventList;
		} catch (PDOException $e) {
			throw $e;
		}
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
		$event = null;


		try {
			Event::verifyId($id);

			$sql = 'SELECT * FROM event WHERE id = ?';
			$stmt = $this->db->prepare($sql);
			$stmt->execute([$id]);
			$tempEvent = $stmt->fetch(PDO::FETCH_ASSOC);
			
			$event = new Event($tempEvent['title'], $tempEvent['date'], $tempEvent['description'], $tempEvent['id']);

			return $event;
		}
		catch (PDOException $e) {
			throw $e;
		}

    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
		try {
			$event->verify(true);

			$title = $event->title;
			$date = $event->date;
			$description = $event->description;

			
			$sql = 'INSERT INTO event(title, date, description) VALUES(?, ?, ?)';
			$stmt = $this->db->prepare($sql);
			$stmt->execute([$title, $date, $description]);

			$event->id = $this->db->lastInsertId();
			
		}
		catch(PDOException $e){
			throw $e;
		}
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {

        try {
		    $event->verify(true);

		    $id = $event->id;
		    $title = $event->title;
		    $date = $event->date;
		    $description = $event->description;


			$sql = 'UPDATE event SET title = ?, date = ?, description = ? WHERE id = ?';
			$stmt = $this->db->prepare($sql);
			$stmt->execute([$title, $date, $description, $id]);

		} catch (PDOException $e) {
		    throw $e;
		}


    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {


		try {
		    Event::verifyId($id);

		    $sql = 'DELETE FROM event WHERE id = ?';
		    $stmt = $this->db->prepare($sql);
		    $stmt->execute([$id]);
		
		} catch (PDOException $e) {
			throw $e;
		}
    }
}
